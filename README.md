# harelang test

try some [hare](https://harelang.org/) (now that it's been [officially
announced](https://harelang.org/blog/2022-04-25-announcing-hare/)).

to run the program, use `hare run main.ha`, to build the program and then run
it, use `hare build -o <name> && ./<name>`.

or just have a look at [the fine
tutorial](https://harelang.org/tutorials/introduction) yourself to learn more.
